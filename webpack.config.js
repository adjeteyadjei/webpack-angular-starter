module.exports = {
	entry: "./app/entry.js",
	output: {
		path: __dirname,
		filename: "./app/bundle.js"
	},
	module: {
		loaders: [
			{test: /\.css$/, loader:"style!css"},
			{test: /\.scss$/, loader: 'style!css!sass'},
			{
        test: /\.js$/,
        loader: 'ng-annotate!babel!jshint',
        exclude: /node_modules|bower_components/
      },
			{
        test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?name=res/[name].[ext]?[hash]'
      },
      {
        test: /\.html/,
        loader: 'raw'
      },
      {
        test: /\.json/,
        loader: 'json'
      }
		]
	}
};