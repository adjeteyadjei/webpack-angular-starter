import {AppCtrl} from './controllers/app_ctrl';

var mod = angular.module("app.controllers", []);
mod.controller('AppCtrl', AppCtrl);