require ("../node_modules/angular-material/angular-material.css");
require("./style.scss");
require('angular');
require('angular-aria');
require('angular-animate');
require('angular-material');

//document.write(require("./content.js"));
var appModule = require('./index');  
// replaces ng-app="appName"
angular.element(document).ready(function () {  
  angular.bootstrap(document, [appModule.name], {
    //strictDi: true
  });
});
